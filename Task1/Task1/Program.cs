﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {

        private static readonly List<(Uri, string)> GoodList = new List<(Uri, string)>();
        private static readonly Queue<Uri> PagesToGet = new Queue<Uri>();

        public const int PagesNumber = 10;

        static void Main(string[] args)
        {
            Console.Write("Enter url: ");
            string arg = Console.ReadLine();
            
            if (arg == null)
            {
                throw new ArgumentException("1 argument required");
            }

            var uri = new Uri(arg);
            var pageCounter = 0;
            while (true)
            {
                pageCounter++;

                if (GoodList.Count > PagesNumber)
                    break;

                if (uri == null)
                {
                    if (!PagesToGet.TryDequeue(out var newUri))
                    {
                        Console.WriteLine("cant get the page");
                    }

                    uri = newUri;
                }

                if (GoodList.Any(a => a.Item1 == uri))
                    continue;

                //get html in content
                string content;
                var myWebRequest = WebRequest.Create(uri);
                var myWebResponse = myWebRequest.GetResponse();
                var myStreamResponse = myWebResponse.GetResponseStream();
                var streamReader = new StreamReader(myStreamResponse);
                content = streamReader.ReadToEnd();

                //add new links in queue
                var links = GetNewLinks(uri, content);
                foreach (var link in links)
                    PagesToGet.Enqueue(link);

                content = RemoveHtmlTags(content);

                if (GetWordsCount(content) > 100)
                {
                    GoodList.Add((uri, content));

                    string fileDirectory = @"C:\Users\User\Desktop\ass\";
                    string fileName = fileDirectory + pageCounter + ".txt";
                    ProcessWrite(fileName, content).Wait();
                }

                uri = null;
            }
        }

        static Task ProcessWrite(string filePath, string text)
        {
            return WriteTextAsync(filePath, text);
        }

        static async Task WriteTextAsync(string filePath, string text)
        {
            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }

        private static string RemoveHtmlTags(string content)
        {
            const string pattern = @"<(.|\n)*?>";

            var step1 = Regex.Replace(content, "<script.*?script>", " ", RegexOptions.Singleline);
            var step2 = Regex.Replace(step1, "<style.*?style>", " ", RegexOptions.Singleline);
            var step3 = Regex.Replace(step2, "&#.*?;", "");
            var step4 = Regex.Replace(step3, pattern, string.Empty);
            var step5 = step4.Replace("\t", "");
            var textOnly = Regex.Replace(step5, "[\r\n]+", "\r\n");
            return textOnly;
        }

        private static int GetWordsCount(string s)
        {
            var splitters = new[]
            {
                '\n', '\t', '\r', ':', ';', '(', ')', '.', ',', ' ', '[', ']', '-', '"', '{', '}', '!', '?', '@', '$', '='
            };
            return s.Split(splitters).Count();
        }

        private static IEnumerable<Uri> GetNewLinks(Uri baseUrl, string content)
        {
            var regexLink = new Regex("(?<=<a\\s*?href=(?:'|\"))[^'\"]*?(?=(?:'|\"))");

            ISet<Uri> newLinks = new HashSet<Uri>();
            foreach (var match in regexLink.Matches(content))
            {
                var value = match.ToString();
                if (value == null)
                {
                    continue;
                }

                Uri uri = null;

                try
                {
                    var temp = new Uri(value, UriKind.RelativeOrAbsolute);
                    uri = !temp.IsAbsoluteUri
                        ? new Uri(baseUrl.GetLeftPart(UriPartial.Authority).TrimEnd('/') + '/' +
                                  temp.ToString().TrimStart('/'))
                        : temp;
                }
                catch
                {
                    // ignored
                }

                if (uri == null)
                    continue;

                if (!newLinks.Contains(uri))
                {
                    newLinks.Add(uri);
                }
            }
            return newLinks;
        }
    }
}